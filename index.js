class Grade {
    constructor(level) {
        this.level = level;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }

    addSection(sectionName) {
        const section = new Section(sectionName);
        this.sections.push(section);
    }

    countStudents() {
        this.totalStudents = 0;
        for (const section of this.sections) {
            this.totalStudents += section.students.length;
        }
    }

    countHonorStudents() {
        this.totalHonorStudents = 0;
        for (const section of this.sections) {
            for (const student of section.students) {
                if (student.isHonorStudent()) {
                    this.totalHonorStudents++;
                }
            }
        }
    }

    computeBatchAve() {
        let totalGrades = 0;
        let totalStudents = 0;
        
        for (const section of this.sections) {
            for (const student of section.students) {
                totalGrades += student.computeAverageGrade();
                totalStudents++;
            }
        }

        if (totalStudents > 0) {
            this.batchAveGrade = totalGrades / totalStudents;
        }
    }

    getBatchMinGrade() {
        let minGrade = Number.MAX_VALUE;

        for (const section of this.sections) {
            for (const student of section.students) {
                minGrade = Math.min(minGrade, student.getMinGrade());
            }
        }

        this.batchMinGrade = minGrade;
    }

    getBatchMaxGrade() {
        let maxGrade = Number.MIN_VALUE;

        for (const section of this.sections) {
            for (const student of section.students) {
                maxGrade = Math.max(maxGrade, student.getMaxGrade());
            }
        }

        this.batchMaxGrade = maxGrade;
    }
}

class Section {
    constructor(name) {
        this.name = name;
        this.students = [];
    }

    addStudent(name, email, grades) {
        const student = new Student(name, email, grades);
        this.students.push(student);
    }
}

class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.grades = grades;
    }

    computeAverageGrade() {
        return this.grades.reduce((sum, grade) => sum + grade, 0) / this.grades.length;
    }

    isHonorStudent() {
        return this.computeAverageGrade() >= 90;
    }

    getMinGrade() {
        return Math.min(...this.grades);
    }

    getMaxGrade() {
        return Math.max(...this.grades);
    }
}


// Creating grade instances and adding sections
const grade1 = new Grade(1);
grade1.addSection("1A");
grade1.addSection("1B");
grade1.addSection("1C");
grade1.addSection("1D");

// Adding students to sections
const section1A = grade1.sections[0];
const section1B = grade1.sections[1];
const section1C = grade1.sections[2];
const section1D = grade1.sections[3];

section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

// Perform calculations
grade1.countStudents();
grade1.countHonorStudents();
grade1.computeBatchAve();
grade1.getBatchMinGrade();
grade1.getBatchMaxGrade();

console.log(grade1);